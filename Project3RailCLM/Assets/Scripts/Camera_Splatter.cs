﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Camera_Splatter : MonoBehaviour {

	public float timeOfSplatter;
	private float timeLeft;
	private float alphaLevel;
	public Image splatterUI;

	// Use this for initialization
	void Start () {
		timeLeft = timeOfSplatter;
	}

	// Update is called once per frame
	void Update () {
		CameraSplatter ();
	}

	public void CameraSplatter(){
		if (timeLeft > 0) {
			alphaLevel = timeLeft / timeOfSplatter;
			splatterUI.color = new Color(splatterUI.color.r, splatterUI.color.g, splatterUI.color.b, alphaLevel);
			timeLeft -= (1 * Time.deltaTime);
		} else if (timeLeft < 0) {
			timeLeft = 0;
			splatterUI.color = new Color(splatterUI.color.r, splatterUI.color.g, splatterUI.color.b, 0);
		}
	}
}
